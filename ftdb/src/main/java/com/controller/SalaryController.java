/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.context.SpringContextUtils;
import com.mapper.Salary;
import com.pojo.SalaryBranchCount;
import com.service.impl.SalaryServiceImpl;

@RestController
public class SalaryController {

    private final Logger logger = LoggerFactory.getLogger(SalaryController.class); 

	

    @Autowired
    private Salary salary;
    
    @RequestMapping("/salary")
    public String salary(){
        ExecutorService executor = Executors.newFixedThreadPool(1);
	    List<SalaryBranchCount> branchCountList = salary.countBranchSalary();
        SalaryServiceImpl salaryServiceImpl = SpringContextUtils.getBean(SalaryServiceImpl.class);
        salaryServiceImpl.setBranchCountList(branchCountList);
        executor.submit(salaryServiceImpl);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error(" SalaryController salary thread InterruptedException {} ", e);
        }
        return "代发工资测试结束";
    }
}
