/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pojo.TranLogStatsDTO;
import com.service.TranLogStatsService;

@RestController
public class TranLogStatsController {

    @Autowired
    TranLogStatsService tranLogStatsServiceImpl;

    @RequestMapping(value = "/sequence-time")
    public @ResponseBody
    Map<String, List<Integer>> getSequenceAndTimeCost(@RequestParam(value = "process_id")String process_id) {
        return tranLogStatsServiceImpl.getTmcostList(process_id);
    }

    @RequestMapping(value = "/time-count")
    public @ResponseBody
    Map<String, List<TranLogStatsDTO>> getTimeCostCount(@RequestParam(value = "process_id")String process_id) {
        return tranLogStatsServiceImpl.getTmcostCount(process_id);
    }
}
