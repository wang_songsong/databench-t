/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.common.fastjson.JsonResult;
import com.pojo.Clustercfg;
import com.pojo.ProcessList;
import com.pojo.ProcessListVo;
import com.service.ProcessListService;

@RestController
public class GetTaskStatusController {
	
	@Autowired
    private ProcessListService processListService;
	
	@RequestMapping("/getProcessStatus")
	public JsonResult getInitStatus(@Param(value = "uuid")String uuid){
		ProcessList processList = processListService.getProcessStatus(uuid);
		return JsonResult.success(processList);
	}
	
	@RequestMapping("/getProcessList")
	public String getProcessList(){
		List<ProcessListVo> processList = processListService.getProcessList();
		return JSON.toJSONString(processList);
	}
	@RequestMapping("/getClusterList")
	public String getClusterList(){
		List<Clustercfg> clusterListList = processListService.getClusterList();
		return JSON.toJSONString(clusterListList);
	}
}
