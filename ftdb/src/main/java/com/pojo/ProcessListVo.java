/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

public class ProcessListVo {

	private String process_id;
	
	private String datacfg_content;
	
	private String process_content;
	
	private String process_time;

	public String getProcess_id() {
		return process_id;
	}

	public void setProcess_id(String process_id) {
		this.process_id = process_id;
	}

	public String getDatacfg_content() {
		return datacfg_content;
	}

	public void setDatacfg_content(String datacfg_content) {
		this.datacfg_content = datacfg_content;
	}

	public String getProcess_content() {
		return process_content;
	}

	public void setProcess_content(String process_content) {
		this.process_content = process_content;
	}

	public String getProcess_time() {
		return process_time;
	}

	public void setProcess_time(String process_time) {
		this.process_time = process_time;
	}
	
	
}
